<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns="http://www.cfs++.org/simulation" 
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
xsi:schemaLocation="http://www.cfs++.org/simulation 
https://opencfs.gitlab.io/cfs/xml/CFS-Simulation/CFS.xsd">
    <!-- define which files are needed for simulation input & output-->
    <fileFormats>
        <input>
            <!-- specify your ANSYS-cdb input file here -->
            <cdb fileName="beam.cdb"/>
        </input>
        <output>
            <hdf5/>
            <text id="txt"/>
        </output>
        <materialData file="mat.xml" format="xml"/>
    </fileFormats>

    <!-- material assignment -->
    <domain geometryType="3d">
        <variableList>
            <!-- here you can specify variables for later use, e.g. in load definitions -->
            <!-- reserved are x,y,z (spacial postion), t (time) and f (frequency) -->
            <var name="T" value="0.1"/>
        </variableList>
        <regionList>
            <region name="V_beam" material="alu"/>
        </regionList>
        <nodeList> <!-- CFS can pick nodes from the mesh, e.g. to write text output for this point -->
            <nodes name="P_end">
                <coord x="0.75" y="0" z="0"/> <!-- this should be the end of the beam, CFS will search for the closest node -->
            </nodes>
        </nodeList>
    </domain>


    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <mechanic subType="3d">
                <regionList>
                    <region name="V_beam"/>
                </regionList>
                <bcsAndLoads>
                    <!-- define boundary conditions here -->
                    <fix name="S_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <!-- uniform pressure load on top >
                    <pressure name="S_top" value="2.0e+5"/>-->
                    <!-- Gravity load rho*g in negative z direction -->
                    <forceDensity name="V_beam">
                        <comp dof="z" value="-9.81*2700"/>
                    </forceDensity>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <nodeList>
                            <nodes name="P_end"/>
                        </nodeList>
                    </nodeResult>
                    <elemResult type="mechStress">
                        <allRegions/>
                    </elemResult>
                    <regionResult type="mechDeformEnergy">
                        <allRegions outputIds="txt"/>
                    </regionResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>

    <sequenceStep index="2">
        <analysis>
            <eigenFrequency>
                <isQuadratic>no</isQuadratic>
                <!-- we want to solve an ordinary generalised EVP -->
                <numModes>10</numModes>
                <!-- compute the first 10 modes -->
                <freqShift>0</freqShift>
                <!-- closest to the shift point (frequency) -->
                <writeModes normalization="max">yes</writeModes>
                <!-- scale them to a maximum displacement of 1 -->
            </eigenFrequency>
        </analysis>
        <pdeList>
            <mechanic subType="3d">
                <regionList>
                    <region name="V_beam"/>
                </regionList>
                <bcsAndLoads>
                    <!-- define boundary conditions here -->
                    <fix name="S_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>

    <sequenceStep index="3">
        <analysis>
            <harmonic>
                <numFreq>15</numFreq>
                <startFreq>5</startFreq>
                <stopFreq>400</stopFreq>
                <sampling>linear</sampling>
            </harmonic>
        </analysis>
        <pdeList>
            <mechanic subType="3d">
                <regionList>
                    <region name="V_beam" dampingId="myDamping"/>
                </regionList>
                <dampingList>
                    <rayleigh id="myDamping">
                        <adjustDamping>yes</adjustDamping>
                        <!-- adjust alpha and beta to get a frequency-constant loss factor = lossTangensDelta in material definition (only works for harmonic analysis) -->
                    </rayleigh>
                </dampingList>
                <bcsAndLoads>
                    <!-- define boundary conditions here -->
                    <fix name="S_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <!-- put an (harmonically oscillating) pressure load on top -->
                    <pressure name="S_top" value="0.2e+5"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <nodeList>
                            <nodes name="P_end"/>
                        </nodeList>
                    </nodeResult>
                    <elemResult type="mechStress">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>

    <sequenceStep index="4">
        <analysis>
            <transient>
                <numSteps>400</numSteps>
                <deltaT>0.25e-2</deltaT>
            </transient>
        </analysis>
        <pdeList>
            <mechanic subType="3d">
                <regionList>
                    <region name="V_beam" dampingId="myDamping"/>
                </regionList>
                <dampingList>
                    <rayleigh id="myDamping">
                        <ratioDeltaF>0.01</ratioDeltaF>
                    </rayleigh>
                </dampingList>
                <bcsAndLoads>
                    <!-- define boundary conditions here -->
                    <fix name="S_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                        <comp dof="z"/>
                    </fix>
                    <pressure name="S_top" value="0.2e+5*(1-exp(-(t/(0.5*T))^2))"/>
                </bcsAndLoads>
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <nodeList>
                            <nodes name="P_end"/>
                        </nodeList>
                    </nodeResult>
                    <elemResult type="mechStress">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>

</cfsSimulation>
