# Editing the Userdocumentation

## Starting the virtual mkDocs-Server

* **Under Ubuntu**: Open the terminal and go to the path where your local repository is stored and execute `# mkdocs serve`
* **Under Windows**: Open the anaconda promt and go to the path where your local repository is stored and execute `A# mkdocs serve`

This generates a virtual Server, which can be visited with a browser (just like an ipython-notebook).
Just copy the http address [http://127.0.0.1:8000](http://127.0.0.1:8000) in your browser and the documentation is displayed.

Leave the browser open and start editing the markdown-files in the /docs folder.
By saving these files, the browser-page gets automatically updated...what you see is what you get principle (no compiling or building needed)
## Basic Structure
The basic structure of the docu is defined in the .yml-file `mkdocs.yml` which is located in the top level folder. Everything else is located in the `/docs` folder, otherwise mkdocs cannot build it.

### Adding a Section/Subsection
If you need to create a new category, simply add these in the `mkdocs.yml` in the navigation section.
```yml
#This is a comment
#Naviation section. Insert here new sections/subsections.
nav:
    - openCFS: 'index.md' 
    - History: 'history.md'
    #Freshly created Section
    - My Section:
    	#Undersection with the path to the markdown-file
    	#Note that the folder 'MySection' needs to be created in '/docs/'						
        - My new Undersection: 'MySection/myMarkdownfile.md'	
    - Installation: 
        - Overview: 'Installation/README.md'
        - Pre-Processors: 'Installation/PrePorcessors.md'
        - XML Editors: 'Installation/XML-Editors.md'
        - openCFS: 'Installation/CFS.md'
        - ParaView Plugin: 'Installation/ParaView.md'

```
If needed create a new subfolder in `/docs` and copy your `Markdownfile.md` in this directory.
These changes are displayed live on the hosted mkdocs server as well.


### Using Jupyter Notebook
One can use jupyter notebooks directly to create markdownfiles. This is especially helpful to see if mathematical expressions are displayed correctly. You can use the same syntax you would use for a latex file. For example:
```latex
\begin{equation}
\int_a^b \frac{1}{x} dx
\end{equation}
```
will be displayed as
\begin{equation}
\int_a^b \frac{1}{x} dx
\end{equation}
 Code snipplets can be displayed as well. And if you click the button on the right side of the snipplet, you simply copy it to you clipboard.
```python
list1 = [i for i in range(10) if i < 5]
list2 = [i**i for i in list]
```
To export the jupyernotebook to a markdown-file simply go to:

 * _File-> Export Notebook as... -> Export Notebook as Markdown_

Now you have created a markdownfile, which only needs to be put into the right place.
Alternativly you can use any texteditor to edit a markdown-file.

