
// Definition of constants
overall = 10;
pmlW = 1;
plateW = 0.1;
plateH = 3.5;
platePosX = 2;
platePosY = 2.25;
elemSize = 0.1;  


// Bottom up modeling approach for structured mesh: Points → lines → surfaces → volumes → mesh

// Define basepoint for plate
basePoint = -overall/2+pmlW+platePosY;
Point(1) = {-overall/2, -overall/2 , 0, 1.0};
Point(2) = {-overall/2, -overall/2+pmlW , 0, 1.0};
Point(3) = {-overall/2, basePoint , 0, 1.0};
Point(4) = {-overall/2, basePoint+plateH , 0, 1.0};
Point(5) = {-overall/2, overall/2-pmlW , 0, 1.0};
Point(6) = {-overall/2, overall/2 , 0, 1.0}; 

// Define lines
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};

// Extrusion leads to mis-oriented elements but CFS++ will correct this later on
Extrude{ pmlW, 0, 0 }{ Line{1,2,3,4,5}; }
Extrude{ platePosX, 0, 0 }{ Line{6,10,14,18,22}; }
Extrude{ plateW, 0, 0 }{ Line{26,30,34,38,42}; }
Extrude{ overall-2*pmlW-platePosX-plateW, 0, 0 }{
  Line{46,50,54,58,62};
} 
Extrude{ pmlW, 0, 0 }{ Line{66,70,74,78,82}; }

// Get rid of multiple entities and current states
Coherence;


// Generate regions to address later in CFS++
Physical Surface("prop") = {33,53,73,38,77,41,61,81,37};
Physical Surface("plate") = {57};
Physical Surface("pml") = {
 9,29,49,69,89,93,97,101,105,85,65,45,25,21,17,13
}; 


// Structured meshing and recombine triangles to quadrilaterals

Transfinite Surface '*';
Recombine Surface '*';

Transfinite Line {7,8,12,16,20,24,87,88,92,96,100,104} = ((pmlW)/elemSize) Using Progression 1;
Transfinite Line {27,28,32,36,40,44} = ((platePosX)/elemSize) Using Progression 1;
Transfinite Line {47,48,52,56,60,64} = ((plateW)/elemSize) Using Progression 1;
Transfinite Line {67,68,72,76,80,84} = ((overall-2*pmlW-platePosX-plateW)/elemSize) Using Progression 1;


// Perform 2D meshing with 1st order elements
Mesh.ElementOrder = 1;
Mesh 2; 











