# Synthetic noise generation and radiation (SNGR) source computation

This filter is based on the theory of C. Bailly for the SNGR [@bechara1994stochastic]. Currently, this method is under development. For further details [@weitz2019stochastic,@weitz2019numerical], the implementation status, and the application please contact  [Stefan Schoder](https://online.tugraz.at/tug_online/visitenkarte.show_vcard?pPersonenId=1D95C1A49837524D&pPersonenGruppe=3). For further development and future publications on the topic, we are happy to colaborate wit your research unit (company or university).


## Acknowledgement
Please provide an **acknowledgement** at the end of your publication using this software part for simulations

_The computational results presented have been achieved [in part] using the software openCFS [SNGR]._

# References
\bibliography
